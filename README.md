## Clone and Run IPYNB File

This repository contains an IPython Notebook file that you can clone and run on your local machine. The repository can be found at [https://gitlab.com/tinoxn/client-segmentation.git](https://gitlab.com/tinoxn/client-segmentation.git).

Follow the steps below to clone the repository and run the IPython Notebook:

1. **Clone the Repository:**

   Clone the repository using the following command in your terminal or command prompt:

   ```shell
   git clone https://gitlab.com/tinoxn/client-segmentation.git
cd client-segmentation

## How to Run an IPython Notebook (`.ipynb`) File

To run an IPython Notebook file, you can use Jupyter Notebook or JupyterLab. Here's a step-by-step guide:

1. **Install Jupyter Notebook**: If you haven't installed Jupyter Notebook yet, you can do so by running the following command in your terminal or command prompt:
```pip install jupyter``
3. **Launch Jupyter Notebook**: Once Jupyter Notebook is installed, navigate to the directory where the cloned repository is located using the terminal or command prompt. Then, run the following command:

This command will start the Jupyter Notebook server and open a new tab in your web browser.

4. **Access and run the notebook**: In the web browser, you will see the Jupyter Notebook interface, displaying the contents of the directory where you launched the server. Navigate to the cloned repository directory and click on the `.ipynb` file you want to run. It will open in a new tab.

5. **Execute the notebook**: Inside the notebook, you'll see a series of code cells. To execute a code cell, select it, and either click the "Run" button in the toolbar at the top of the page or press "Shift + Enter" on your keyboard. The code will run, and the output, if any, will be displayed below the cell.

6. **Proceed through the notebook**: Continue running each code cell in the notebook sequentially until you've gone through the entire document. Some cells may depend on the execution of previous cells, so make sure to maintain the correct order.

That's it! You can now run and interact with IPython Notebook files using Jupyter Notebook or JupyterLab. Remember to save your changes if you make any modifications to the notebook during your session.


# Machine learning- Group 17
## Title: Customer Segmentation 

### Section 1: Introduction
The goal of this project is to identify the optimal number of segments and assign each user to one of them to facilitate targeted marketing campaigns.

### Section 2: Methodology
In this section, we are going to describe the methods and steps taken in this project.

**Data cleaning:**
We first loaded our dataset, which is customer segmentation, using pandas and then checked whether our dataset has missing values. We found that there are no missing values in the dataset. We used `data.isna().sum` to check for missing values. We also checked for outliers depending on the price variable but decided to keep them.

**Customer segmentation method:**
RFM analysis (Recency, Frequency, Monetary) & cluster analysis. RFM analysis can help identify the most valuable customers based on their purchase behavior, while cluster analysis can help uncover patterns and segments in the data and then categorize them. We used 4 categories: At-Risk Customers, Potential Loyalists Customers, New Customers, and Loyal Customers.

### Section 3: Data analysis and data visualization
We conducted descriptive analysis to analyze each numerical variable in our dataset. Then, we performed multivariate analysis to assess the correlation among variables. We created a histogram for single variables like customer states to see how customers are distributed among states. Additionally, we decided to create a pie-chart of payment categories to assess which payment method is used the most.

### Section 4: Feature Engineering
We created new features to be used for recency, frequency, and monetary value. For recency, we used `order_purchase_timestamp` to create a recency feature (variable) representing the number of days since a customer's last purchase. 
Next, we created a frequency feature, which counts the number of times each customer made a purchase. We extracted this feature from the `customer_id` variable.
The last feature we created is monetary value, which is the sum of spending for each customer. We calculated this feature by combining `customer_id` and `payment_value`, then summing them.

### Section 5: Results of segments created and discussion
We determined the optimal number of segments using the elbow curve. We decided to use 4 segments.

**Segments interpretation:**
We conducted an analysis based on the revenue contribution of each segment. We used a pie-chart to assess the percentage of contribution.

**INTERPRETATION:** Results show that At-Risk Customers have the highest percentage, while Loyal Customers have the lowest.

### Section 6: Conclusion and Recommendation
Based on the analysis of our segments created from customers, we can recommend the following actions for each segment:

**At-Risk Customers:**
1. Develop a loyalty program with tiered benefits.
2. Offer exclusive discounts or promotions for At-Risk Customers.
3. Personalize their experience through targeted marketing campaigns as they generate more income for the company.

**Potential Loyal Customers:**
1. Send personalized emails or offers to encourage them to return and make a purchase.
2. Understand the reasons behind their decreased frequency and address any concerns.
3. Offer incentives or rewards for repeat purchases to increase their engagement.

**New Customers:**
1. Send a welcome email or offer to thank them for their business.
2. Encourage them to make another purchase through personalized recommendations.
3. Provide a seamless onboarding experience and address any questions or concerns.

**Loyal Customers:**
1. Reward their loyalty with exclusive discounts or promotions.
2. Personalize their experience through targeted marketing campaigns.
3. Encourage referrals or word-of-mouth recommendations.

### Section 7: Contribution of group members
In this project In this project we collaborated as we did all parts of the project together in person and online from the starting phase to the end. 

**Niyonshuti Valentin(771761):** Designed the project plan of our project.Implemented and evaluated the RFM Method and clustering. designed the insights of the results. Responsible for making the presentation.
 
**Eddy Ndateba Mandela(771351):** Created plots and charts to visualize the data and the model performance.  Responsible for data preprocessing(cleaning) and Developed code for cleaning the data and extracting new features. Responsible for writing the technical report

The project was easy and possible as a result of each and everyone's contribution. 


